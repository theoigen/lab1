import car.Car;

public class Main {
    public static void main(String[] args) {
        Car car_1 = new Car();
        System.out.println("Is car currently running: " + car_1.isCarRunning());
        car_1.start();
        System.out.println("Is car currently running: " + car_1.isCarRunning());
    }
}
