package car;

public class Car {
    private boolean isRunning;

    public Car() {
        this.isRunning = false;
    }

    public void start() {
        this.isRunning = true;
    }

    public void stop() {
        this.isRunning = false;
    }

    public boolean isCarRunning() {
        return this.isRunning;
    }
}
